
var dump = require('../myconfig/override.js' ).dump;
exports.get = function (req, res, next) {
	var settings = res.locals._admin.settings,
		custom = res.locals._admin.custom;

	var groups = {};
	var tables = [];
	for (var key in settings) {
		var item = settings[key];
		if (!item.mainview.show || !item.table.pk || item.table.view) continue;
		if (item.group) {
			if (!groups[item.group]) {
				groups[item.group] = [];
			}
			groups[item.group].push({slug: item.slug, name: item.table.verbose});
		} else {
			tables.push({slug: item.slug, name: item.table.verbose});
		}
	}


/*
	for (var key in settings) {
		var item = settings[key];
		if (!item.mainview.show || !item.table.pk || item.table.view) continue;
		tables.push({slug: item.slug, name: item.table.verbose});
	}
*/
	var views = [];
	for (var key in settings) {
		var item = settings[key];
		if (!item.mainview.show || !item.table.view) continue;
		views.push({slug: item.slug, name: item.table.verbose});
	}

	var customs = [];
	for (var key in custom) {
		var item = custom[key].app;
		if (!item || !item.mainview || !item.mainview.show) continue;
		customs.push({slug: item.slug, name: item.verbose});
	}
	//AML!!
	res.locals.groups = [];
	for (var key in groups) {
		var items = groups[key];
		res.locals.groups.push( {groupname:key, items:items} );
	}
	res.locals.tables = !tables.length ? null : {items: tables};
	res.locals.views = !views.length ? null : {items: views};
	res.locals.custom = !customs.length ? null : {items: customs};
	
	res.locals.partials = {
		content:  'mainview'
	};

	next();
}

/*
 preList args

 name - this table's name
 slug - this table's slug
 filter - filter data submitted via POST request
 columns - list of columns (and their values) to filter by
 direction - sort order direction
 order - column names by which to order
 or - true|false whether to use logical or or not
 statements - sql query strings partials
 columns - columns to select
 table - table to select from
 join - join statements
 where - where statements
 group - group by statements
 order - order statements
 from - limit from number
 to - limit to number
 db - database connection instance
*/

exports.preList = function (req, res, args, next) {

    if (args.debug) console.log('preList');
    debugger;

    if (args.name == 'go_team') {
        console.log('\npreList got go_team\n'.red);
    }

    if (args.name == 'purchase') {
        // check if we're using listview's filter
        // and actually want to see soft deleted records
        var filter = args.filter.columns;
        if (filter && (filter.deleted=='1' || filter.deleted_at && filter.deleted_at[0])) {
            return next();
        }
        // otherwise hide the soft deleted records by default
        var filter = quotes(args,
            ' `purchase`.`deleted` IS NULL OR `purchase`.`deleted` = ' +
            (args.db.client.pg ? 'false' : 0) +
            ' OR `purchase`.`deleted_at` IS NULL ');
        args.statements.where
            ? args.statements.where += ' AND ' + filter
            : args.statements.where = ' WHERE ' + filter
    }

    next();
}
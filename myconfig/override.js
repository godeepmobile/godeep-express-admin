var _ = require('underscore');
var util = require('util');
var project = require('../lib/app/project');

exports.users = function (args) {
    project.createUser(args.dpath, {name:'mike', pass:'peedog'}, function(err) {
       if (err) console.log('Error creating extra user', err);
    });
    project.createUser(args.dpath, {name:'mark', pass:'peedog'}, function(err) {
        if (err) console.log('Error creating extra user', err);
    });
}

exports.settings = function (allTables) {
	var thisTable;
    var field = null;


    ////////////////////////////////////////////////////////////////////////////
    // hidden tables
    ////////////////////////////////////////////////////////////////////////////

    thisTable = allTables.pg_stat_statements;
    if (thisTable) {
        thisTable.mainview.show = false;
    }

    thisTable = allTables.player_view;
    if (thisTable) {
        thisTable.mainview.show = false;
    }

    thisTable = allTables.lb_acl;
    if (thisTable) {
        thisTable.mainview.show = false;
    }

    thisTable = allTables.lb_accesstoken;
    if (thisTable) {
        thisTable.mainview.show = false;
    }

    thisTable = allTables.view_team_play_data;
    if (thisTable) {
        thisTable.mainview.show = false;
    }

    thisTable = allTables.view_go_team;
    if (thisTable) {
        thisTable.table.pk = "id";
        thisTable.table.verbose = "team view";
        thisTable.mainview.show= false;
    }

    thisTable = allTables.view_go_event;
    if (thisTable) {
        thisTable.mainview.show= false;
    }

    thisTable = allTables.view_team_player;
    if (thisTable) {
        thisTable.table.pk = "id";
        thisTable.table.verbose = "player view";
        thisTable.mainview.show= false;
    }

    ////////////////////////////////////////////////////////////////////////////
    // system entities
    ////////////////////////////////////////////////////////////////////////////
    thisTable = allTables.go_conference;
    if (thisTable) {
        thisTable.group = 'System Entities';
        //thisTable.listview.view = '../../../conference_listview';
        //thisTable.editview.view = '../../../conference_editview';
        thisTable.table.verbose = "conference";
        thisTable.listview.order = { "name": "asc"};
    }


    thisTable = allTables.go_user;
    if (thisTable) {
        thisTable.table.verbose = "user";
        showManyToOne(allTables, thisTable, "go_user_email", "go_user_id");
        //showManyToOne(allTables, thisTable, "go_user_team_assignment", "go_user_id");
        hideAllColumnsExcept(thisTable, ["user_name","first_name","last_name"]);
        thisTable.group = "System Entities";
    }

    thisTable = allTables.go_team;
    if (thisTable) {
        thisTable.table.verbose = "team";
        thisTable.group = "System Entities";
        showOneToOne(allTables, thisTable, "team_configuration", "go_team_id");

        //showManyToOne(allTables, thisTable, "team_player", "go_team_id");
        showManyToOne(allTables, thisTable, "team_position_type", "go_team_id");
        showManyToOne(allTables, thisTable, "team_practice_drill_type", "go_team_id");
        showManyToOne(allTables, thisTable, "team_position_group", "go_team_id");
        hideAllColumnsExcept(thisTable, ["go_organization_id","short_name","abbreviation","go_sport_type_id", "go_conference_id"]);
        setFieldVerboseName(thisTable, "short_name", "short name");
    }

    thisTable = allTables.go_user_email;
    if (thisTable) {
        thisTable.table.verbose = "user email";
        thisTable.group = "System Entities";
    }

    thisTable = allTables.go_user_team_assignment;
    if (thisTable) {
        thisTable.table.verbose = "user team assignment";
        thisTable.group = "System Entities";
        hideColumns(thisTable, ["principal_type"]);
    }

    thisTable = allTables.go_organization;
    if (thisTable) {
        thisTable.table.verbose = "organization";
        //showManyToOne( allTables, thisTable, "go_team", "go_organization_id" );
        thisTable.group = "System Entities";
        setFieldVerboseName(thisTable, "short_name", "short name");
        setFieldVerboseName(thisTable, "espn_name", "ESPN name");
        setFieldVerboseName(thisTable, "asset_base_url", "asset base URL");
        hideAllColumnsExcept(thisTable, ["name","short_name","go_conference_id"]);
    }

    thisTable = allTables.go_game_event;
    if (thisTable) {
        thisTable.table.verbose = "scheduled game";
        thisTable.group = "System Entities";
    }


    ////////////////////////////////////////////////////////////////////////////
    // system picklists
    ////////////////////////////////////////////////////////////////////////////

    thisTable = allTables.go_organization_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "organization type";
	}


	thisTable = allTables.go_play_result_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "result";
	}


	thisTable = allTables.go_body_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "body type";
	}


	thisTable = allTables.go_state;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "state";
	}


	thisTable = allTables.go_user_role_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "user role";
        thisTable.listview.order = { "name": "asc"};
	}

    thisTable = allTables.go_platoon_type;
    if (thisTable) {
        thisTable.group = 'System Picklists';
        thisTable.table.verbose = "platoon type";
        thisTable.listview.order = { "name": "asc"};
    }

    thisTable = allTables.go_role_permission;
    if (thisTable) {
        thisTable.group = 'System Picklists';
        thisTable.table.verbose = "role permission";
        thisTable.listview.order = { "go_user_role_type_id":"asc" };
    }


    thisTable = allTables.go_permission;
    if (thisTable) {
        thisTable.group = 'System Picklists';
        thisTable.table.verbose = "permission";
        thisTable.listview.order = { "id": "asc"};
    }


    thisTable = allTables.go_sport_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "sport";
	}


	thisTable = allTables.go_position_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "position";
        setFieldVerboseName(thisTable, "short_name", "short name");
	}


	thisTable = allTables.go_field_condition;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "field condition";
	}


	thisTable = allTables.go_surface_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "surface";
	}


	thisTable = allTables.go_event_type;
	if (thisTable) {
        thisTable.group = 'System Picklists';
		thisTable.table.verbose = "event type";
        setFieldVerboseName(thisTable, "short_name", "short name");
    }


    ////////////////////////////////////////////////////////////////////////////
    // team details
    ////////////////////////////////////////////////////////////////////////////

    thisTable = allTables.team_player;
    if (thisTable) {
        thisTable.table.pk = "id";
        thisTable.table.verbose = "player";
        thisTable.group = "Team Details";
        hideAllColumnsExcept(thisTable, ["last_name", "first_name", "jersey_number", "go_team_id"]);
    }

    thisTable = allTables.team_configuration;
    if (thisTable) {
        thisTable.table.verbose = "configuration";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_event;
    if (thisTable) {
        thisTable.table.verbose = "event";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_play_grade;
    if (thisTable) {
        thisTable.table.verbose = "play grade";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_play_data;
    if (thisTable) {
        thisTable.table.verbose = "play data";
        thisTable.group = "Team Details";
        hideColumns(thisTable, ["data", "team_practice_drill_type_id", "go_play_result_type_id"]);
    }

    thisTable = allTables.team_player_position_assignment;
    if (thisTable) {
        thisTable.mainview.show = false;
        thisTable.table.verbose = "player position assignment";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_platoon_type;
    if (thisTable) {
        thisTable.mainview.show = false;
        thisTable.table.verbose = "platoon";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_position_type;
    if (thisTable) {
        thisTable.table.verbose = "position";
        thisTable.group = "Team Details";
        setFieldVerboseName(thisTable, "short_name", "short name");
    }

    thisTable = allTables.team_practice_drill_type;
    if (thisTable) {
        thisTable.table.verbose = "practice drill";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_position_group;
    if (thisTable) {
        thisTable.table.verbose = "position group";
        thisTable.group = "Team Details";
    }

    thisTable = allTables.team_player_career_stat;
    if (thisTable) {
        thisTable.table.verbose = "player career stats";
        thisTable.group = "Team Details";
        hideAllColumnsExcept(thisTable, ["team_player_id", "go_team_id"]);
    }

    thisTable = allTables.team_player_season_stat;
    if (thisTable) {
        thisTable.table.verbose = "player season stats";
        thisTable.group = "Team Details";
        hideAllColumnsExcept(thisTable, ["team_player_id", "go_team_id","season"]);
    }

    thisTable = allTables.team_player_game_stat;
    if (thisTable) {
        thisTable.table.verbose = "player game stats";
        thisTable.group = "Team Details";
        hideAllColumnsExcept(thisTable, ["team_player_id","go_team_id","season","team_event_id", "date"]);
    }

    thisTable = allTables.team_user_position_group_assignment;
    if (thisTable) {
        thisTable.table.verbose = "user position group assignment";
        thisTable.group = "Team Details";
    }

    for (var key in allTables) {
        thisTable = allTables[key];
        var id = _.findWhere(thisTable.columns, {name: "id"});
        if (id && (id.type.indexOf("int") > -1)) {
            id.editview.show = false;
            id.listview.show = false;
        }
        setAllOneToMany(thisTable);
    }
}


function showOneToOne ( allTables, thisTable, otherTableName, idFieldName ) {
    if ( !thisTable.editview.oneToOne ) {
        thisTable.editview.oneToOne = {};
    }
    thisTable.editview.oneToOne[otherTableName] = idFieldName;

    // turn off the foreign key field in the other table in the edit view
    var otherTable = allTables[otherTableName];

    // hide the other table in the main list of tables
    otherTable.mainview.show= false;
    var field = _.findWhere(otherTable.columns, {name: idFieldName});
    if (field) {
        field.editview.show = false;
    }
}


function showManyToOne( allTables, thisTable, otherTableName, idFieldName ) {
    // create the object if it doesn't exist, use it it does
    if ( !thisTable.editview.manyToOne ) {
        thisTable.editview.manyToOne = {};
    }

    // show multiple values for otherTable foreign key
    thisTable.editview.manyToOne[otherTableName] = idFieldName;
    
    // turn off the foreign key field in the other table in the edit view
    var otherTable = allTables[otherTableName];

    // hide the other table in the main list of tables
    otherTable.mainview.show= false;

    var field = _.findWhere(otherTable.columns, {name: idFieldName});
    if (field) {
        field.editview.show = false;
    }
}


function showOneToMany( field, table, columns ) {
	field.control = { select : true };
	field.oneToMany = {
		table: table,
		pk: "id",
		columns: columns
	}

}


var oneToManyFields = [
    //name, verbose, table, select fields
    ['go_organization_id','organization','go_organization',['name']],
    ['go_organization_type_id','organization type','go_organization_type',['name']],
    ['go_conference_id','conference','go_conference',['name']],
    ['go_team_id','team','view_go_team',['short_name']],
    ['away_team_id','away team','view_go_team',['short_name']],
    ['home_team_id','home team','view_go_team',['short_name']],
    ['go_user_role_type_id','user role','go_user_role_type',['name']],
    ['go_user_id','user','go_user',['user_name']],
    ['go_sport_type_id','sport','go_sport_type',['name']],
    ['go_field_condition_id','field condition','go_field_condition',['name']],
    ['go_surface_type_id','surface','go_surface_type',['name']],
    ['go_event_type_id','event type','go_event_type',['name']],
    ['go_position_type_id','position','go_position_type',['name']],
    ['go_platoon_type_id','platoon','go_platoon_type',['name']],
    //['go_user_team_assignment_id','user team assignment','view_go_user_team_assignment',['description']],
    ['team_position_type_id','position','team_position_type',['name']],
    ['team_position_group_id','position group','team_position_group',['name']],
    ['go_play_result_type_id','result','go_play_result_type',['name']],
    ['go_game_event_id','scheduled game','view_go_event',['date','description']],
    ['team_event_id','team event','team_event' ,['date','description']],
    ['team_play_data_id','play','view_team_play_data',['description','play_number']],
    ['team_player_id','player','team_player',['last_name','first_name']],
    ['go_permission_id','permission','go_permission',['description']],
    ['team_practice_drill_type_id','practice drill','team_practice_drill_type',['name']]
];


function setAllOneToMany(thisTable) {
    var item;
    for (var x in oneToManyFields) {
        item = oneToManyFields[x];
        var field = _.findWhere(thisTable.columns, {name: item[0]});
        if (field) {
            field.verbose = item[1];
            showOneToMany( field, item[2], item[3] );
        }
    }
}
function hideColumns(thisTable, hiddenColumns) {
    _.each( thisTable.columns, function(column, index, list) {
        if (_.contains(hiddenColumns,column.name)) {
            column.listview.show = false;
        }
    },this);
}

function hideAllColumnsExcept(thisTable, exceptions) {
    _.each( thisTable.columns, function(column, index, list) {
        if (!_.contains(exceptions,column.name)) {
            column.listview.show = false;
        }
        console.log('hideAllColumnsExcept %s: %s is %s', thisTable.table.name, column.name, column.listview.show ? "shown" : "hidden");
    },this);
}


function setFieldVerboseName(thisTable, originalName, verboseName) {
    var field = _.findWhere(thisTable.columns, {name: originalName});
    if (field) {
        field.verbose = verboseName;
    }
}


function dump( name, obj ) {
	console.log("%s = %s", name, util.inspect(obj, { depth: null }));
}

exports.dump = dump;